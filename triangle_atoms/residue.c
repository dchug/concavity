/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   residue.c
 * Author: ketan
 *
 * Created on May 24, 2016, 9:30 AM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
char* getToken(char buffer[],int start,int len)
{
    char token[30];
    int i=0;int index=0;
    for(i=start;i<(start+len);i++)
    {
      token[index] = buffer[i];  
      index++;
    }
    
    token[index] = 0;
    
    return token;
}

int main(int argc, char** argv) {

    
    FILE* residue = fopen("1G6C_test1_residue.pdb","r");
    
    char rbuffer[1024];
    
    
    float x[1024], y[2014],z[1024];
    int index=0;
    
    //Goes through each atoms residue score
    //If residue is higher than .6 than store x,y,z coordinates into array
    while (fgets(rbuffer, 1024, residue))
    {
        float score = atof(getToken(rbuffer,62,4));
        if(score>.6)
        {
            x[index] = atof(getToken(rbuffer,31,6));
            y[index] = atof(getToken(rbuffer,39,6));
            z[index] = atof(getToken(rbuffer,47,6));
            
            index++;
            
        printf("\n score %f",score);
        }
    }
    fclose(residue);
    /* Once we have all atoms with the score we need look up if they are in triangle
     * Triangle atoms needs to have all the atoms residue scores greater than .6
     * next part of code should go this way 
     * 1. Find the x,y,z in the output file and 
     * 2. Once the x,y,z are matched than check if the other x,y,z of the atom are in the array
     * 3. If they are than check if we have been through these or not by having an another array
     * 4. Another array will store the index which have already been through
     * 
     */
    //array to keep the record of those atoms which have already been considered
    int coordinated[1024]; 
    FILE* tri = fopen("output.txt","r");
    char tbuffer[1024];
    
    while(fgets(rbuffer, 1024, residue))
    {
        
    }
    fclose(tri);
    
    return (EXIT_SUCCESS);
}

