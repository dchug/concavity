/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: ketan
 *
 * Created on May 11, 2016, 12:49 PM
 */

/*
 command to run the file: gcc main.c -o main -lm
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


/*
 * 
 */
FILE *fp;
FILE *fp1;
FILE *fp2;
float getDistance(float x,float y,float z,float x1,float y1,float z1)
{
    float result;
    float a = x1-x;a = a*a;
    float b = y1-y;b = b*b;
    float c = z1-z;c = c*c;
    
    result = (a+b+c);
    result = sqrt(result);
    
    
    return result;
    
}

char* getToken(char buffer[],int start,int len)
{
    char token[30];
    int i=0;int index=0;
    for(i=start;i<(start+len);i++)
    {
      token[index] = buffer[i];  
      index++;
    }
    
    token[index] = 0;
    
    return token;
}

float getRadii(char atom)
{
    float radius;
    switch(atom)
    {
        case 'C':
            radius = .68;
            break;
        case 'O':
            radius = .68;
            break;
        case 'N':
            radius = .68;
            break;
        case 'S':
            radius = 1.02;
            break;
        default:
            radius = 0.00;
            break;
    }
    return radius;
    
}
int checkTriangle(float a,float b,float c)
{
    if((a>2 && a<=10) && (b>2 && b<=10))
    {
        if(c>10 && c<=13)
        {
            return 1;
        }
    }else if((c>2 && c<=10) && (b>2 && b<=10))
    {
        if(a>10 && a<=13)
        {
            return 1;
        } 
    }
    else if((c>2 && c<=10) && (a>2 && a<=10))
    {
       if(b>10 && b<=13)
        {
            return 1;
        }  
    }
    else
    {
        return 0;
    }
        
}

int main(int argc, char** argv) {
    
    
    fp = fopen("1G6C.pdb","r");
    //FILE *fp1 = fopen("1G6C.pdb","r");
    char buffer[1024];
    char buffer1[1024];
    char buffer2[1024];
    int i=0;
    char result[1024];
    /*these are to take care of name of atoms.
     First attempt is on carbon atoms so these variables are not so much used 
     * but in case of other atoms only these variables will be used
     */
    char atom1,atom2,atom3;
    FILE* fp3 = fopen("output.txt","w+");
    while (fgets(buffer, 1024, fp))
    {
       // printf("\n chain name %c ", buffer[21]);
        char chain = buffer[21];        
        int count=0,count1=0;
        if(chain!=32 && buffer[13]=='C' && (!strncmp(&buffer[0], "ATOM  ", 6)))
        {
            fp1 = fopen("1G6C.pdb","r");
            
            float x = atof(getToken(buffer,31,6));
            float y = atof(getToken(buffer,39,6));
            float z = atof(getToken(buffer,47,6));
            
            atom1 = toupper(buffer[13]);
            //float r = getRadii(atom1);
           // printf("\n Atom %c",buffer[13]);
            
            while(fgets(buffer1,1024,fp1))
            {
                if(chain == buffer1[21] && buffer1[13]=='C' && (!strncmp(&buffer1[0], "ATOM  ", 6)))
                {
                 
                    if(chain==buffer1[21] && count==0){
                        count++;
                    }
                    atom2 = toupper(buffer1[13]);
                    
                    float x1 = atof(getToken(buffer1,31,6));
                    float y1 = atof(getToken(buffer1,39,6));
                    float z1 = atof(getToken(buffer1,47,6));
                    //check if it's not the same atom
                    
                    if(x!=x1 && y!=y1 && z!=z1)
                    {
                        float dis01 = getDistance(x,x1,y,y1,z,z1);
                        if(dis01<=13 && dis01>2)
                        {
                            fp2 = fopen("1G6C.pdb","r");
                            while (fgets(buffer2, 1024, fp2))
                            {
                                if(chain==buffer2[21] && buffer2[13]=='C' && (!strncmp(&buffer2[0], "ATOM  ", 6)))
                                {
                                     if(chain==buffer1[21] && count1==0){
                                        count1++;
                                     }
                                    atom3 = toupper(buffer2[10]);
                                    float x2 = atof(getToken(buffer2,31,6));
                                    float y2 = atof(getToken(buffer2,39,6));
                                    float z2 = atof(getToken(buffer2,47,6));
                                    
                                    if((x1!=x2 && y1!=y2 && z1!=z2) && (x!=x2 && y!=y2 && z!=z2))
                                    {
                                      float  dis02 = getDistance(x,x2,y,y2,z,z2);
                                      float  dis12 = getDistance(x1,x2,y1,y2,z1,z2);
                                      
                                      if(checkTriangle(dis01,dis02,dis12))
                                      {
                                       
                                       fprintf(fp3,"\n%c\t%.3f,%.3f,%.3f \t %.3f,%.3f,%.3f \t %.3f,%.3f,%.3f",chain,x,y,z,x1,y1,z1,x2,y2,z2);
                                       
                                      }
                                      
                                      
                                    }
                                }
                                else if(chain != buffer2[21] && count1==1)
                                {
                                     break;  
                                     count1=0;
                                }
                            }
                            fclose(fp2);
                        }
                         //  printf("connected "); 
                    }
                }
                else if(chain != buffer1[21] && count==1)
                {
                    break;
                    count=0;
                }
                    //float cons =4.000;
                    //distance = sqrt(cons);
                    //float r2 = getRadii(buffer[10])
                    //printf("\n %f",r);
                    //if()
                    
                    //printf("\n x coordinate %f",x);
                    //int x = atof();
                    // if(!strncmp(&buffer[0], "ATOM  ", 6)){
                    // int len = strlen(buffer);
      
                    // char altLoc[5] ;
                    // strncpy(altLoc,getToken(buffer,21,3),4);
                    // printf("altLoc is %s \n",altLoc);
                    //printf("%d",len);
                }
            fclose(fp1);
        }
        
            }
    fclose(fp3);

            
        
    
    
    return (EXIT_SUCCESS);
    }



